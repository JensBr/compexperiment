import java.rmi.Remote;
import java.rmi.RemoteException;

public interface Compute extends Remote {
	<T> T executeTask(Task<T> t) throws RemoteException;
	public float add(float x, float y) throws RemoteException;
	public float mul(float x, float y) throws RemoteException;
	public float pow(float x, float y) throws RemoteException;
}