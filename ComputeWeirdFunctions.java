import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class ComputeWeirdFunctions {
	public static void main(String[] args) {
//		if (System.getSecurityManager() == null) {
//			System.setSecurityManager(new SecurityManager());
//		}
		try {
			float a = 10, b = 5;
			String name = "ComputeWeirdFunctions";
			Registry registry = LocateRegistry.getRegistry(1099);
			Compute comp = (Compute) registry.lookup(name);
			//Formel: 5* ((a-b) + (-a/b))
			float c = comp.mul(5, comp.add(comp.add(a, comp.mul(b, -1)), comp.mul(a, comp.pow(b, -1))));
			System.out.println("Ergebnis: "+c);
//			WeirdFunctions wFTask = new WeirdFunctions(Integer.parseInt(args[1])); // TODO
//			Float result = (Float) comp.executeTask(wFTask); // TODO
		} catch (Exception e) {
			System.out.println("ComputeWeirdFunctions Exception: \n");
			e.printStackTrace();
		}
	}
	
	public float evaluate(String formula){
		for (int i = 0; i < formula.length(); i++) {
			switch (formula.charAt(i)) {
			case '0': return 0;
			case '1': return 1;
			case '2': return 2;
			case '3': return 3;
			case '4': return 4;
			case '5': return 5;
			case '6': return 6;
			case '7': return 7;
			case '8': return 8;
			case '9': return 9;
			case '(': return -1; // TODO
			case ')': return -1; // TODO
			case '+': return -1; // TODO
			case '-': return -1; // TODO
			case '*': return -1; // TODO
			case '/': return -1; // TODO
			case '^': return -1; // TODO
			
				
			default: 
				System.out.println("Nicht zugelassenes Zeichen erkannt");
				break;
			}
		}
		return 0;
	}
	

}
