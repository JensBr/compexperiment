
import java.rmi.RemoteException;
import java.rmi.registry.Registry;
import java.rmi.registry.LocateRegistry;
import java.rmi.server.UnicastRemoteObject;


public class ComputeEngine implements Compute {
	
	public ComputeEngine() throws RemoteException {
		super();
	}
	
	@Override
	public <T> T executeTask(Task<T> t){
		return t.execute();
	}
	
	@Override
	public float add(float x, float y) throws RemoteException {
		return x + y;
	}
	
	@Override
	public float mul(float x, float y) throws RemoteException {
		return x * y;
	}
	
	@Override
	public float pow(float x, float y) throws RemoteException {
//		float e = 1;
//		if (y == 0) return 1;
//		for (int i = 1; i <= y; i++) {
//			e = e * x;
//		}
//		return 1;
		return (float)Math.pow(x, y);
//		return e;
	}
	
	public static void main(String[] args) {
//		if (System.getSecurityManager() == null) {
//			System.setSecurityManager(new SecurityManager());
//		}
		
		try {
			String name = "ComputeWeirdFunctions";
			Compute engine = new ComputeEngine();
			Compute stub = (Compute) UnicastRemoteObject.exportObject(engine, 0);
			System.setProperty("java.rmi.server.hostname","127.0.1.1");
			Registry registry = LocateRegistry.createRegistry(1099);
			registry.rebind(name, stub);
			System.out.println("ComputeEngine bound");
			
		} catch (Exception e) {
			System.out.println("ComputeEngine Exception:");
			e.printStackTrace();
		}
	}

}
